<!doctype html>
<html>
<head>
  <title>Pendu app: interface d'admin</title>
  <style>
    * {
      font-family:helvetica, sans;
      font-size:24dip;
      background-color:#222;
      color:#DDD;
      margin-top:2%;
    }
    .f1{
      float:left;
      line-height:2em;
      margin-left:20%;
    }
    .f2{
      float:right;
      line-height:2em;
      margin-right:20%;
    }

    @media screen and (max-width:1250px){
      .f1{
        float:none;
      }
      .f2{
        margin-left:20%;
        float:none;
        margin-right:20%;
      }
}
  </style>
</head>
<body>
  <div class="f1">
    <ul>
    <?php
      ini_set("display_errors" . "true");
      $handle = mysqli_connect("localhost","root","1234","PenduApp");
      $query = "SELECT * FROM joueurs";
      $result = mysqli_query($handle,$query);
      echo "Il y a " . $result->num_rows . " joueurs" . "<br>";
      while($line = mysqli_fetch_array($result)) {
        echo "<li>" . $line["id"] . " ";
        echo $line ["nom"];
        echo "&nbsp;<a href=\"delete.php?id=" . $line["id"] . "\">X</a>";
        echo "&nbsp;<a href=\"update.php?id=" . $line["id"] . "\">U</a>" . "</li>";
      }
    ?>
    </ul>
    <form action="create_motd.php" method="post">
      <input name="mot2" type="text">
      <label for="mot2">nouveau joueur</label>
      <input type="submit">
    </form>
    </div>
  <div class="f2">
    <ul>
    <?php
      ini_set("display_errors" . "true");
      $handle = mysqli_connect("localhost","root","1234","PenduApp");
      $query = "SELECT * FROM mots";
      $result = mysqli_query($handle,$query);
      echo "Il y a " . $result->num_rows . " mots" . "<br>";
      while($line = mysqli_fetch_array($result)) {
        echo "<li>" . $line["id"] . " ";
        echo $line ["mot"];
        echo "&nbsp;<a href=\"delete.php?id=" . $line["id"] . "\">X</a>";
        echo "&nbsp;<a href=\"update2.php?id=" .$line["id"] . "\">U</a>" . "</li>";
      }
    ?>
    </ul>
    <form action="create_mot.php" method="post">
      <input name="mot" type="text">
      <label for="mot">nouveau mot</label>
      <input type="submit">
    </form>
  </div>
</body>
</html>
