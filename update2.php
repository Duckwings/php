<!doctype html>
<html>
<head>
  <meta charset="utf-8">
  <title>Update</title>
  <style>
  * {
    margin-top:5%;
    font-family:helvetica, sans;
    background-color:#222;
    color:#DDD;
    text-align:center;
  }

  a {
    font-size:2.3em;
  }
  </style>
</head>
<body>
  <form action="update_mot.php" method="GET">
     <label for="mot">Modifier mot par :</label>
     <input name="mot" type="text" placeholder="Votre nouveau nom" tabindex="1" autocomplete="off">
     <input type="hidden" name="id" value="<?php echo $_GET["id"]; ?>" >
     <input type="submit">
  </form>
</body>
</html>
